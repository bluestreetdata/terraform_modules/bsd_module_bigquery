


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bq\_location | The location of the datasets | `string` | `"australia-southeast1"` | no |
| labels | labels to be applied | `map` | <pre>{<br>  "org": "bsd"<br>}</pre> | no |
| project\_id | The project in which the datasets will be created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| company\_prefix | The Random company prefix assigned |
| project\_id | The ID of the project in which resources are provisioned. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->