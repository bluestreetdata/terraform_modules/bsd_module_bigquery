# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_bigquery/-/tree/v0.1.1) - 2022-08-24
### Changes
- Updated developer version to 1

## [0.1.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_bigquery/-/tree/v0.1.0) - 2022-01-04

### Added
- Optional datasets can be created
- Option to not create common datasets
- temp dataset will now expire
- labels added to common datasets

## [0.0.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_bigquery/-/tree/v0.0.0) - 2021-03-21

### Features

- Initial release



