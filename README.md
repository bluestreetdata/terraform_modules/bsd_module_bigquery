# bsd-bigquery

This module was generated from [bsd-module-template](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_tempate), which by default creates a collection of datasets in bigquery. As the module develops, this README should be updated.

The resources/services/activations/deletions that this module will create/trigger are:

- 3 datasets (staging, metadata and temp) with a given company prefix

## Usage

Basic usage of this module is as follows:

```hcl
module "bsd_bigquery" {
  source = "git::git@gitlab.com:bluestreetdata/terraform_modules/bsd_module_bigquery.git//?ref=tags/v0.0.1"
  project_id  = "<PROJECT ID>"
  company_prefix = "<COMPANY PREFIX>"
}
```

Functional examples are included in the
[examples](./examples/) directory.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bq\_location | The location of the datasets - defaults to australia-southeast1 | `string` | `"australia-southeast1"` | no |
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| create\_common\_datasets | Defaults to true, will create metadata, temp and staging datasets | `bool` | `true` | no |
| extra\_datasets | A mapping of extra datasets to be created | `map(any)` | `{}` | no |
| project\_id | The project\_id in which the datasets will be created | `any` | n/a | yes |

## Outputs

No output.

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Requirements

These sections describe requirements for using this module.

### Software

The following dependencies must be available:

- [Terraform][terraform] v0.12
- [Terraform Provider for GCP][terraform-provider-gcp] plugin v2.0



