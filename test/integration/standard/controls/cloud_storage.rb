# copyright: 2018, The Authors
title 'Verifying the cloud storage has been set up correctly'

DATA_BUCKET_NAME = attribute('data_bucket_name',description: 'the name of the created data bucket')
CODE_BUCKET_NAME = attribute('code_bucket_name',description: 'the name of the created code bucket')
GCS_LOCATION = attribute('gcs_location', description: 'The default location of gcs storage')

control "data-bucket" do
 title 'Ensure data Bucket is available and has versioning disabled.'

  describe google_storage_bucket(name: DATA_BUCKET_NAME) do
    it { should exist }
    its('storage_class') { should eq "STANDARD" }
    its('location') { should eq GCS_LOCATION }
    its('labels') { should eq ({'org' => 'bsd', 'tf' => 'cloud_storage', 'use' => 'data'})}
    its('versioning.enabled') { should eq nil}
  end
end

control "code-bucket" do
  title 'Ensure the code bucket has been set up correctly'

  describe google_storage_bucket(name: CODE_BUCKET_NAME) do
    it { should exist }
    its('storage_class') { should eq "STANDARD" }
    its('location') { should eq GCS_LOCATION }
    its('labels') { should eq ({'org' => 'bsd','tf' => 'cloud_storage', 'use' => 'code'})}
    its('versioning.enabled') { should eq nil}
  end
end