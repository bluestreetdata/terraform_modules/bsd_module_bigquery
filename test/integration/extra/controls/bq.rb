title 'Verifying the big query has been set up correctly'

BQ_LOCATION = attribute('bq_location', description: 'The default location of gcs storage')
PROJECT = attribute('project', description: 'The project the datasets are in')
COMPANY_PREFIX = attribute('company_prefix', description: 'The random prefix')

control "metadata-not-present" do
    title 'Ensure the metadata dataset has been not been made made ok.'   
        describe google_bigquery_dataset(project: PROJECT, name: COMPANY_PREFIX + '_metadata') do
            it { should_not exist }
        end
end

control "optional" do
    title 'Ensure the optional datasets has been made made ok.'
    ['horses', 'dogs'].each do |dataset|
        describe google_bigquery_dataset(project: PROJECT, name: dataset) do
            it { should exist }
            its('location') { should eq BQ_LOCATION }
        end
    end
end