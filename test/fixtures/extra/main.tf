/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# provider "random" {
#   version = "~> 2.0"
# }

locals {
  extra_datasets = {
    "horses" = {
      dataset_id    = "horses",
      friendly_name = "horses",
      description   = "Dataset for holding horses"
    },
    "dogs" = {
      dataset_id    = "dogs",
      friendly_name = "dogs",
      description   = "Dataset for holding dogs"
    },
  }
}

resource "random_string" "company_prefix" {
  length  = 3
  lower   = true
  special = false
  number  = false
  upper   = false
}


module "extra_datasets" {
  source                 = "../../../examples/extra"
  project_id             = var.project_id
  company_prefix         = random_string.company_prefix.id
  create_common_datasets = false
  extra_datasets         = local.extra_datasets
}
