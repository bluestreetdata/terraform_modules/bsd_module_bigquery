# Standard Example

This example illustrates how to use the `bsd_module_bigquery` module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bq\_location | The location of the datasets | `string` | `"australia-southeast1"` | no |
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| project\_id | The project in which the datasets will be created | `any` | n/a | yes |

## Outputs

No output.

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
