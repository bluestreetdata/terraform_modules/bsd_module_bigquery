variable "project_id" {
  description = "The project_id in which the datasets will be created"
}

variable "bq_location" {
  description = "The location of the datasets - defaults to australia-southeast1"
  default     = "australia-southeast1"
}

variable "create_common_datasets" {
  type        = bool
  default     = true
  description = "Defaults to true, will create metadata, temp and staging datasets"

}

variable "company_prefix" {
  type        = string
  description = "the unique prefix for resource naming"
}


variable "extra_datasets" {
  type        = map(any)
  description = "A mapping of extra datasets to be created"
  default = {
  }
}
