locals {
  labels = {
    tf : "{cookiecutter.module_name}"

  }
  ds_labels = { org : var.company_prefix }

  common_datasets = {
    "staging" = {
      dataset_id                  = "${var.company_prefix}_staging",
      friendly_name               = "${var.company_prefix}_staging",
      description                 = "Dataset for holding bla bla",
      default_table_expiration_ms = 0,
      labels                      = { purpose : "staging" }
    },
    "metadata" = {
      dataset_id                  = "${var.company_prefix}_metadata",
      friendly_name               = "${var.company_prefix}_metadata",
      description                 = "Dataset for holding metadata related tables",
      default_table_expiration_ms = 0,
      labels                      = { purpose : "metadata" }
    }

  }

  expire_datasets = {
    "temp" = {
      dataset_id                  = "${var.company_prefix}_temp",
      friendly_name               = "${var.company_prefix}_temp",
      description                 = "Dataset for holding temp tables will expire in an hour",
      default_table_expiration_ms = 3600000,
      labels                      = { purpose : "temp" }
    },

  }
}

resource "google_bigquery_dataset" "common_dataset" {
  for_each      = var.create_common_datasets ? local.common_datasets : {}
  project       = var.project_id
  dataset_id    = each.value.dataset_id
  friendly_name = each.value.friendly_name
  description   = each.value.description
  location      = var.bq_location
  labels        = merge(local.ds_labels, each.value.labels)
}


resource "google_bigquery_dataset" "common_dataset_expire" {
  for_each                    = var.create_common_datasets ? local.expire_datasets : {}
  project                     = var.project_id
  dataset_id                  = each.value.dataset_id
  friendly_name               = each.value.friendly_name
  description                 = each.value.description
  location                    = var.bq_location
  default_table_expiration_ms = each.value.default_table_expiration_ms
  labels                      = merge(local.ds_labels, each.value.labels)
}


resource "google_bigquery_dataset" "extra_dataset" {
  for_each      = var.extra_datasets
  project       = var.project_id
  dataset_id    = each.value.dataset_id
  friendly_name = each.value.friendly_name
  description   = each.value.description
  location      = var.bq_location
}
