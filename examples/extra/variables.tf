/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "The project in which the datasets will be created"
}

variable "bq_location" {
  description = "The location of the datasets"
  default     = "australia-southeast1"
}


# variable "labels" {
#   description = "labels to be applied"

# }

variable "company_prefix" {
  type        = string
  description = "the unique prefix for resource naming"
}

variable "extra_datasets" {
  type        = map(any)
  description = "A mapping of extra datasets to be created"

}

variable "create_common_datasets" {
  type        = bool
  description = "Determine if to create the common datasets or not"

}
