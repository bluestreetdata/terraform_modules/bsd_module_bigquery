title 'Verifying the big query has been set up correctly'

BQ_LOCATION = attribute('bq_location', description: 'The default location of gcs storage')
PROJECT = attribute('project', description: 'The project the datasets are in')
COMPANY_PREFIX = attribute('company_prefix', description: 'The random prefix')
control "standard-datasets" do
    title 'Ensure the standard datasets has been made ok.'
    [COMPANY_PREFIX + '_metadata',COMPANY_PREFIX + '_temp',COMPANY_PREFIX + '_staging'].each do |dataset|
        describe google_bigquery_dataset(project: PROJECT, name: dataset) do
            it { should exist }
        
            its('friendly_name') { should eq dataset }
            its('location') { should eq BQ_LOCATION }
            # its('description') { should eq 'Test BigQuery dataset description' }
            # its('name') { should eq 'inspec_gcp_dataset' }
            # its('default_table_expiration_ms') { should cmp '3600000' }
        end
    end
end

control "temp-dataset" do
    title 'Ensure the temp datasets have an expiry set'
    [COMPANY_PREFIX + '_temp'].each do |dataset|
        describe google_bigquery_dataset(project: PROJECT, name: dataset) do
            its('default_table_expiration_ms') { should cmp '3600000' }
        end
    end
end